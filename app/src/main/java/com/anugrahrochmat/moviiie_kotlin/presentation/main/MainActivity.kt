package com.anugrahrochmat.moviiie_kotlin.presentation.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.anugrahrochmat.moviiie_kotlin.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
