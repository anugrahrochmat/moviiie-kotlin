package com.anugrahrochmat.moviiie_kotlin.presentation.splashscreen

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.anugrahrochmat.moviiie_kotlin.R
import com.anugrahrochmat.moviiie_kotlin.presentation.moviecollection.MovieCollectionActivity

class SplashScreenActivity : AppCompatActivity() {

    private var handler: Handler? = null

    private val mRunnable: Runnable = Runnable {
        if (!isFinishing) {

            val intent = Intent(applicationContext, MovieCollectionActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        //Initialize the Handler
        handler = Handler()

        //Navigate with delay
        handler!!.postDelayed(mRunnable, 4000)

    }

    public override fun onDestroy() {
        if (handler != null) handler!!.removeCallbacks(mRunnable)
        super.onDestroy()
    }
}
