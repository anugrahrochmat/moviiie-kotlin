package com.anugrahrochmat.moviiie_kotlin

import android.app.Application
import timber.log.Timber


/**
 * @author Tyas Anugrah Rochmat (tyas.anugrah@dana.id)
 * @version AndroidApplication, v 0.1 2019-12-07 09:32 by Tyas Anugrah Rochmat
 */
class AndroidApplication : Application() {

    override fun onCreate() {
        super.onCreate()


        initializeTimber()
    }

    private fun initializeTimber() {
        // Timber
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    private fun initializeCanary() {
        // LeakCanary
//        if (LeakCanary.isInAnalyzerProcess(this)) {
//            // This process is dedicated to LeakCanary for heap analysis.
//            // You should not init your app in this process.
//            return
//        }
//        LeakCanary.install(this)
    }
}